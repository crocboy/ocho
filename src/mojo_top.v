module mojo_top(
    // 50MHz clock input
    input clk,
    // Input from reset button (active low)
    input rst_n,
    // cclk input from AVR, high when AVR is ready
    input cclk,
    // Outputs to the 8 onboard LEDs
    output[7:0]led,
    // AVR SPI connections
    output spi_miso,
    input spi_ss,
    input spi_mosi,
    input spi_sck,
    // AVR ADC channel select
    output [3:0] spi_channel,
    // Serial connections
    input avr_tx, // AVR Tx => FPGA Rx
    output avr_rx, // AVR Rx => FPGA Tx
    input avr_rx_busy, // AVR Rx buffer full
	 input reg_in_en, // Reg IN ENABLE
	 input [3:0] reg_in, // Reg IN
	 input reg_out_en, // Reg OUT ENABLE
	 in 
    );
 
wire rst = ~rst_n; // make reset active high
 
// these signals should be high-z when not used
assign spi_miso = 1'bz;
assign avr_rx = 1'bz;
assign spi_channel = 4'bzzzz;

// REG DEMO
register #(.WIDTH(4)) m_reg(.clk(clk), .en_i(reg_in_en), .en_o(reg_out_en), .in(reg_in[3:0]), .out(led[3:0]));

assign led[7:4] = 4'b0;

//SLOW CLOCK DEMO
//slow_clock #(.FACTOR(21)) clock(clk, led[7:0]);


 
endmodule