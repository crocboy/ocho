`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Joey Freeland
// 
// Create Date:    12:03:47 06/29/2014 
// Design Name: 
// Module Name:    register 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: A simple register with active-low input and output enables
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module register #(parameter WIDTH = 8)(
	 input clk,
	 input en_i, // Input enable
	 input en_o, // Output enable
	 input [WIDTH-1:0] in, // Input register
	 output reg [WIDTH-1:0] out // Output register
    );


// Holds the actual data
reg [WIDTH-1:0] data;


// When input enable goes LOW (active)
always @(posedge clk) begin

	if (!en_i) begin
		
		// Update 'data' with the data present on the input lines
      data <= in;
		  
	end
	
	if (!en_o) begin
	
		// Assign the output pin to data
		out <= 13;
		
	end
		  
end


endmodule
