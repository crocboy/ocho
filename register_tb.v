`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:39:03 07/06/2014
// Design Name:   register
// Module Name:   C:/Mojo/Ocho/register_tb.v
// Project Name:  Mojo-Base
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: register
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module register_tb;

	// Inputs
	reg clk;
	reg en_i;
	reg en_o;
	reg [3:0] in;

	// Outputs
	reg [3:0] out;

	// Instantiate the Unit Under Test (UUT)
	register uut (
		.clk(clk), 
		.en_i(en_i), 
		.en_o(en_o), 
		.in(in), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		en_i = 1;
		en_o = 1;
		in = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		in = 15;
		en_i = 0;
		
		en_i = 1;
		en_o = 0;
		
		#1000;

	end
	
	initial begin
	  #10000 $finish;
	end
	
	
	always begin 
     #5  clk =  ! clk;
	end
	  
      
endmodule

