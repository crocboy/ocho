`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Joey Freeland
// 
// Create Date:    14:40:53 06/29/2014 
// Design Name: 
// Module Name:    full_adder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: A full adder of width 'WIDTH'
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module adder #(parameter WIDTH = 8) (
	 
	 input in_a[WIDTH-1:0],
	 input in_b[WIDTH-1:0],
	 output sum[WIDTH-1:0]
    );
	 
// Contain our carries
reg carry_ins[WIDTH-1:0];
reg carry_outs[WIDTH-1:0];

// Define 1 adder
full_adder add0 (
            .a(in_a[0]),
            .b(in_b[0]),
            .c_in(0),
            .sum(sum[0]),
				.c_out(carry_outs[0])
        );
		  
carry_ins[0] = 


genvar i;
generate
    for (i = 1; i < WIDTH; i=i+1) begin: add_gen_loop 
        pwm #(.CTR_LEN(3)) pwm (
            .rst(rst),
            .clk(clk),
            .compare(i),
            .pwm(led[i])
        );
    end
endgenerate



		  
		  
// Define 2 adder
full_adder add0 (
            .a(in_a[i]),
            .b(in_b[i]),
            .c_in(i),
            .sum(led[i]),
				.c_out(led[i])
        );
		  
// Define 3 adder
full_adder add0 (
            .a(in_a[i]),
            .b(in_b[i]),
            .c_in(i),
            .sum(led[i]),
				.c_out(led[i])
        );
		  
// Define 4 adder
full_adder add0 (
            .a(in_a[i]),
            .b(in_b[i]),
            .c_in(i),
            .sum(led[i]),
				.c_out(led[i])
        );
		  
// Define 5 adder
full_adder add0 (
            .a(in_a[i]),
            .b(in_b[i]),
            .c_in(i),
            .sum(led[i]),
				.c_out(led[i])
        );
		  
// Define 6 adder
full_adder add0 (
            .a(in_a[i]),
            .b(in_b[i]),
            .c_in(i),
            .sum(led[i]),
				.c_out(led[i])
        );
		  
// Define 7 adder
full_adder add0 (
            .a(in_a[i]),
            .b(in_b[i]),
            .c_in(i),
            .sum(led[i]),
				.c_out(led[i])
        );
		  



endmodule
