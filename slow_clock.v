`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Joey Freeland
// 
// Create Date:    16:27:30 06/28/2014 
// Design Name: 
// Module Name:    slow_clock 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Slows down the 50 Mhz clock
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module slow_clock #(parameter FACTOR = 24)(
    input clk,
    output out
    );
 
reg [FACTOR:0] counter_d, counter_q;
 
assign out = counter_q[FACTOR];
 
always @(counter_q) begin
    counter_d = counter_q + 1'b1;
end
 

always @(posedge clk) begin
        counter_q <= counter_d;
end
 
endmodule