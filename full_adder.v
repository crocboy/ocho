`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Joey Freeland
// 
// Create Date:    14:40:53 06/29/2014 
// Design Name: 
// Module Name:    full_adder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: A full adder that adds two bits w/ a carry in and carry out
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module full_adder(
	 
	 input a,
	 input b,
	 input c_in,
	 output sum,
	 output c_out
    );

	 
// Logic implementation

wire c1, c2, c3; //wiring needed
assign sum = a ^ b ^ c_in; //half adder (XOR gate)
assign c1 = a * c_in; //carry condition 1
assign c2 = b * c_in; //carry condition 1
assign c3 = a * b; //carry condition 1
assign c_out = (c1 + c2 + c3);


endmodule
